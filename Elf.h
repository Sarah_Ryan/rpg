#pragma once
#include "stdafx.h"
#include "Character.h"
#include <string>

class Elf : public Character {

private:
	Warrior Warrior_Traits;
	Mage Mage_Traits;
	Ranger Ranger_Traits;
	Beast_Tamer Tamer_Traits;


	
	int finalHP;

public:
	Elf();

	int finalDMG;

	int getFinalHP();

};