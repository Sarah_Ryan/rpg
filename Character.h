#pragma once
#include "stdafx.h"
#include <string>

using namespace std;

class Character{

private:
	string charName;

	//Ints representing options
	int charClass;
	int charRace;


	
public:
	Character();

	int str;
	int def;
	int hp;

	string getCharName();
	void setCharName(string theName);

	int getCharClass();
	void setCharClass(int theClass);

	int getCharRace();
	void setCharRace(int theRace);

	int getStr();
	void takeDmg(int amount);
	bool isDead();


	struct Warrior {
		string weapon = "Sword";
		int weaponDMG = 5;
		int hpMod = 10;
	};

	struct Mage {
		string weapon = "Spells";
		int weaponDMG = 6;
	};

	struct Ranger {
		string weapon = "Bow";
		int weaponDMG = 8;
	};

	struct Beast_Tamer {
		string weapon = "Staff";
		int weaponDMG = 4;
	};
};
