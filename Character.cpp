#include "stdafx.h"
#include "Character.h"

Character::Character() {
	charName = "Default";

	charClass = 0;
	charRace = 0;
}

string Character::getCharName() {
	return charName;
}

void  Character::setCharName(string theName) {
	charName = theName;
}

int Character::getCharClass() {
	return charClass;
}

void Character::setCharClass(int theClass) {
	charClass = theClass;
}

int Character::getCharRace() {
	return charRace;
}

void Character::setCharRace(int theRace) {
	charRace = theRace;
}

int Character::getStr() {
	return str;
}

void Character::takeDmg(int amount) {
	hp -= (amount - def);

	if (hp < 0) {
		hp = 0;
	}
}

bool Character::isDead() {
	return (hp <= 0);
}