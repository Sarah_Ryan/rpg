This is a Retro RPG made in C++ and OpenGL by myself for a first year university project. 

There is currently no executable file but the game will build and run in Visual Studio.

The main game is text based and can be played through the console window.
A second window will open with an image of the scene the character is in, you can move the character around using WASD. Currently, there is only OpenGL code for the tavern area.

I had wanted to make one for a couple years before starting university, I really enjoyed making it and am proud. 

Features:

- Classes and races that affect player actions and NPC reactions
- Multiple paths through the story and multiple endings
- Random generation of enemies
- Dragon charming