#pragma once
#include "stdafx.h"
#include <string>

using namespace std;

class Enemy{
private:
	int str;
	int def;
	int hp;

public:
	Enemy();

	int getStr();
	int getDef();
	int getHp();

	void setHp(int theHp);
	void takeDmg(int amount);
	bool isDead();
};