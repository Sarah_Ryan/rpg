#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <vector>
#include <stack>
#include <stdlib.h>
#include <time.h>
#include <sstream>

#include "Character.h"
#include "Human.h"
#include "Elf.h"
#include "Orc.h"
#include "Cat.h"
#include "Enemy.h"


int answer; //Holds value that indicates what action player wants to take
int diceResult; //Holds random number between 1 and 6 - Used to incorporate chance
bool passed; //Holds value that indicates if player passed an action/event

int inventorySpace = 5; //Defines initial inventory space
vector<string> playerInventory(inventorySpace); //Vector acting as player inventory

int passNumber; //Value that the players dice roll has to exceed if they want to pass event/action
Character *playerData; //Pointer initially pointing to the base class that is used to store basic info about the player character

//Function Prototypes

//Clears the screen of text 
void ClearScreen();

//Handles all action user takes in the menu
void Menu();

//Loops through vector and outputs each element
void CheckInventory(vector<string> &inventory);

//Simulates a dice roll and returns the result
int DiceRoll();

//Outputs a short description about an area to the user on screen 
string AreaDescription(string description);

//Lists all the actions the player can take in an area and returns their decision.
int PickAction(string a, string b, string c, string d);

//Function that calls the dice roll function, compares the generated value with the pass value and decides if player passes the action
bool Action(int passValue);

//running condition of game loop
bool gameRunning = true;

bool knowledge = false; //If player asks locals for knowledge of drake, it will aid them in final encounter

//Runs at start, asks player for their character info
void CharCreation();

//Handles all actions that occur in the tavern
void Tavern();

//Handles dialogue with bartender
void TavernBartender();

//Forces player to go and talk with the bartender 
void TalkWithBartender();


//Handles all actions that occur just outside the tavern
void OutsideTavern();


//Handles all actions that occur in the forest 
void Forest();

//Handles actions that happen to player while foraging
void Forage();

//Handles actions that happen to player while chopping wood
void WoodChop();

//Handles actions that happen to player while wandering
void Wander();

//Handles actions that happen to player with animals
void Animals();

//Handles all actions that occur in the market
void Market();

//Handles actions that happen to player while checking stall
void TomeStall();

//Handles actions that happen to player while getting info
void DrakeInfo();

//Handles all actions that occur in the camp
void Camp();


//Handles all actions that occur in the cave
void Cave();

//Handles actions that happen to player while listening in cave
void Listen();


//Handles all actions that occur with the drake
void Drake();

//Handles actions that happen to player while killing drake
void Kill();

//Handles actions that happen to player while charming drake
void Charm();

//Handles actions that happen to player while talking with drake
void Talk();

//Handles actions that happen to player while freezing drake
void Freeze();

	int DiceRoll() {
		srand((unsigned int)time(NULL)); //sets up srand using system time

		diceResult = rand() % 7; //Gets a random number between 1 and 6

		return diceResult; //returns value
	}

	void Combat(int enemyNum) {

		int answer;
		int strength = playerData->getStr(); //gets player strength value from the class.

		//Declares a stack to hold enemy objects in
		stack <Enemy> enemyWave;

		//3 instances of enemies. 
		Enemy en1;
		Enemy en2;
		Enemy en3;


		//pushes enemies onto stack 
		enemyWave.push(en1);
		enemyWave.push(en2);
		enemyWave.push(en3);


		do {

			Enemy topEnemy = enemyWave.top(); //stores the top enemy in the stack in a variable
			int enemyDmg = topEnemy.getStr(); //gets enemy damage from class

			cout << "What will you do?" << endl;
			cout << "Attack (1), Run (2)" << endl;
			cin >> answer;

			if (answer == 1) { //Player attacks....
				cout << "You hit, it does damage!" << endl;
				topEnemy.takeDmg(strength); //Calls takeDmg function present in enemy class, applies damage equal to player damage
				cout << "But you also get hit! You take some damage... just a flesh wound right?" << endl;
				playerData->takeDmg(enemyDmg); //Player also takes damage
				if (playerData->isDead() == true) {
					cout << "You have fallen in battle... Really?.. Okay well you died. Maybe not just a flesh wound..." << endl;
					system("pause");
					Menu();
				}
				if (topEnemy.isDead() == true) { //if enemy is dead
					cout << "One dies! But another steps forward" << endl;
					system("pause");
					ClearScreen();
					enemyWave.pop(); //one enemy removed from stack
				}
			}
			else if (answer == 2) {
				cout << "That's cute, you thought that would work. You're supposed to be an adventurer!! How will you fight a dragon if you run from this??" << endl;

			}

		} while (!enemyWave.empty()); //This combat continues until all enemies have been removed from the stack

		cout << "You killed... everyone. You head towards the cave covered in blood, feel guilty? No? Hmm" << endl;
		system("pause");
		Cave();
	}

	void ClearScreen()
	{  // the \n character is more or less the same as endl except you can have multiple instances and it is quicker
		cout << string(100, '\n'); //This line adds 100 'n' characters so the player cannot see any f the old text.
	}

	void Menu() {
		int userNum;
		char backButton;

		//Tells the user what numbers to press for each menu option
		cout << "Welcome to the game. Press a number from 1 - 3 to select an option" << endl << "Start (1)" << endl << "Instructions (2)" << endl << "Exit (3)" << endl;
		cin >> userNum;


		//his switch case determines what happens when the player selects each menu option
		switch (userNum) {

		case 1: //Start Game
			ClearScreen();
			CharCreation();
			break;

		case 2: //Instructions
			ClearScreen();
			cout << "This game is text based, to play the game you will choose actions (by pressing a button on your keyboard) that you want your character to perform in the world. However, there will be some simple graphics displayed on another screen simultaneously" << endl <<
				"This game has multiple endings and pathways based on the decisions you make starting in character generation" << endl
				<< "Press b to go back to the main menu" << endl;

			cin >> backButton;
			if (backButton = 'b') { //if the player presses b, the game reloads the main menu so they can choose another option
				ClearScreen();
				Menu();
			}
			else { //if they press any other key, the game exits.
				exit(0);
			}

			break;
		case 3: //quit
			exit(0);


		}

	}

	string AreaDescription(string description) {

		cout << description << endl;
		cout << "You can..." << endl;

		return description;

	};


	void CheckInventory(vector<string> &inventory) {
		for (int i = 0; i < 6; i++) { //loops through vector.

			cout << inventory[i] << endl; //outputs each item
		}

		system("pause");
		//Need way to get back to previous menu. 


	}

	bool searchInventory(vector<string> &inventory, string item) {// linear search function
		bool found = false;

		for (int i = 0; i < 6; i++) {
			if (item == inventory[i]) { //goes through each item in the vector and compares it to desired item
				found = true; //item is found
			}
		}

		return found;
	}

	int PickAction(string a, string b, string c, string d) {

		int answer;

		//list all possible actions in situation
		cout << a << endl;
		cout << b << endl;
		cout << c << endl;
		cout << d << endl;

		cout << "Check inventory (5)" << endl;
		cout << "What will you do?" << endl;

		cin >> answer;

		return answer; //returns player's decision
	};

	void CharCreation() {
		string name;

		int characterClass;
		string stringClass;
		int characterRace;
		string race;
		char correct;
		bool correctRace = false;

		cout << "What is your characters name?" << endl;
		cin >> name;

		cout << "Pick a race: Human (1), Elf (2), Orc (3), Cat (4)" << endl;
		cin >> characterRace;

		//Player picks a race and the switch statement assigns it to a local race variable ready to print to the console
		switch (characterRace) {
		case 1:
			race = "Human";
			break;
		case 2:
			race = "Elf";
			break;
		case 3:
			race = "Orc";
			break;
		case 4:
			race = "Cat";
			break;
		}


		cout << "Pick a character class: Warrior (1), Mage (2), Ranger (3), Beast Tamer (4)" << endl;
		cin >> characterClass;

		//Player picks a class and the switch statement assigns it to a local class variable ready to print to the console
		switch (characterClass) {
		case 1:
			stringClass = "Warrior";
			break;
		case 2:
			stringClass = "Mage";
			break;
		case 3:
			stringClass = "Ranger";
			break;
		case 4:
			stringClass = "Beast Tamer";
			break;
		}


		cout << "Name: " + name << endl;
		cout << "Race: " + race << endl;
		cout << "Class: " + stringClass << endl;
		cout << "Is this info correct? (Y/N)" << endl;

		cin >> correct;
		if (correct == 'Y' || 'y') {

			//Checks what the user's race is and creates a new variable accordingly
			if (characterRace == 1) {
				correctRace = true;
				playerData = new Human;
			}
			else if (characterRace == 2) {
				correctRace = true;
				playerData = new Elf;
			}
			else if (characterRace == 3) {
				correctRace = true;
				playerData = new Orc;
			}
			else if (characterRace == 4) {
				correctRace = true;
				playerData = new Cat;
			}

			//sets player's class in the playerData class
			switch (characterClass) {
			case 1:
				playerData->setCharClass(1);
				break;
			case 2:
				playerData->setCharClass(2);
				break;
			case 3:
				playerData->setCharClass(3);
				break;
			case 4:
				playerData->setCharClass(4);
				break;
			}

			//sets variables in playerData class based on decisions made in character creation
			playerData->setCharName(name);
			playerData->setCharRace(characterRace);
			playerData->setCharClass(characterClass);

			ClearScreen();
			Tavern();

		}

		else if (correct == 'N' || correct == 'n') { 
			ClearScreen();
			cout << "Please enter character info again.." << endl;
			system("pause");
			CharCreation();
		}
	};

	void TavernBartender() {
		cout << "The bartender says: Ah! You look like some sort of adventurer! You shall be the one fix our dragon problem! Go now!!."
			"He then marks out a route on a map and hands it to you. You add it to your inventory." << endl;
		cout << "You look at the map, it shows a path through the forest to a huge cave. You need the money so you decided to help the people." << endl;
		playerInventory.insert(playerInventory.begin(), "World Map"); //Adds item to vector
		system("pause");
		ClearScreen();
		OutsideTavern();
	}

	void TalkWithBartender() {
		cout << "You decide to go talk with the bartender before leaving the tavern......" << endl;
		system("pause");
	}

	void TomeStall() {
		ClearScreen();
		if (playerData->getCharClass() == 2) { //Checks players class
			cout << "You look at the books and pick one up. Your mage training is over. You feel a magical energy stir inside you. Maybe you can try casting spells later" << endl;
			system("pause");
		}
		else {
			cout << "You pick up a book... you open it but the pages are blank.. curious" << endl;
			cout << "You decide to put it down and leave the stall" << endl;
			system("pause");
		}
		Market();

	}

	void DrakeInfo() {
		ClearScreen();
		if (playerData->getCharRace() == 2) {
			cout << "You decide to ask the local bounty hunters about they dragon. You learn that there is a legendary sword hidden in the forest" << endl;
			cout << "If you can find it, it will kill the dragon instantly. You also learn there is more than 1 way to deal with the dragon" << endl;
			knowledge = true;
		}
		else {
			cout << "You decide to ask the local bounty hunters about they dragon. You learn dragons like meat... This could aid you in the fight" << endl;
			cout << "You also learn there is more than 1 way to deal with the dragon" << endl;
		}

		system("pause");
		Market();

	}

	void Forage() {
		ClearScreen();
		cout << "You look around in the bushes.... " << endl;

		passed = Action(4);

		if (passed == true) {
			cout << "You find a legendary sword!! It's added to your inventory" << endl;
			playerInventory.insert(playerInventory.begin(), 1, "Boss Sword");
			cout << "Suddenly your vision fades to black..." << endl;
			system("pause");
			Camp();
		}
		else {
			cout << "You find some meat scraps... Maybe you can feed the dragon.." << endl;
			playerInventory.insert(playerInventory.begin(), 2, "Meat scraps");
			cout << "Suddenly your vision fades to black..." << endl;
			system("pause");
			Camp();
		}

	}

	void WoodChop() {
		cout << "You spend the afternoon chopping wood with your sword.... Your sword wears down but you're stronger for it. Every cloud..." << endl;
		cout << "Suddenly your vision fades to black..." << endl;
		system("pause");
		Camp();
	}

	void Wander() {
		if (playerData->getCharClass() == 2 || playerData->getCharRace() == 3) {
			cout << "You wander aimlessly about in the woods, you're experienced and realize the forest is trapped!! You move quickly and cautiously... You reach the cave safely" << endl;
			system("pause");
			Cave();
		}
		else {
			cout << "You wander... but you don't know forests. You're now lost, well done. Someone who does know the forest has been following you..." << endl;
			cout << "Your vision fades to black.... " << endl;
			system("pause");
			Camp();
		}
	}

	void Animals() {
		cout << "You play with the deer, squirrels and other small friendly animals in the forest. You get a bit carried away and don't notice it's night... " << endl;

		if (playerData->getCharClass() == 4 || playerData->getCharRace() == 4) {
			cout << "Although now the animals follow you!" << endl;
			system("pause");
			cout << "The animals scare way any potential threats and you reach the cave safely!" << endl;
			system("pause");
			Cave();
		}

		else {
			cout << "The animals realize it's late too... They all run away home. You have no home here." << endl;
			cout << "Suddenly your vision fades to black..." << endl;
			system("pause");
			Camp();
		}



	}

	void Listen() {
		ClearScreen();
		cout << "You listen in darkness... You hear.. breathing? It's a deep rumble. You move towards the sound" << endl;
		system("pause");
		Drake();
	}

	void Kill() {
		if (searchInventory(playerInventory, "Boss Sword") == true) {
			cout << "You use the legendary sword to stab the dragon through the head.. It worked!! It's very dead, the breathing stops...";
			cout << "Yay! You've completed the demo. Well done!" << endl;
			exit(1);
		}
		else {
			cout << "You stab it with your... starting weapon... That'll totally work. Your weapon and you are ripped to pieces by the dragon, it didn't even eat you" << endl;
			cout << "You have completed the demo... A disappointing end" << endl;
		}

	}

	void Charm() {
		cout << "You wake up the dragon while... stroking and petting him. He seems to like it, things heat up... " << endl;
		cout << "After, you ride the dragon out of the cave and you both find a new hideout far away from people to live out your romance free from judgment from society" << endl;
		cout << "Yay! You've completed the demo. Well done!" << endl;
		exit(2);
	}

	void Talk() {
		if (knowledge == true) {
			cout << "You remember the locals somehow bestowed the ability to speak dragon to you" << endl;
			cout << "You wake up the dragon and before he eats you, you yell something in dragon language. He pauses and looks at you..." << endl;
			cout << "The dragon settles down and you talk with him. He realizes your supreme intelligence and agrees to leave the village alone" << endl;
			cout << "Yay! You've completed the demo. Well done!" << endl;
			exit(3);
		}

		else {
			cout << "You wake the dragon up.... YOU CAN'T SPEAK DRAGON. You panic, it eats you... Good job." << endl;
			cout << "You completed the demo... Not a good ending though." << endl;
			system("pause");
			exit(5);
		}

	}

	void Freeze() {
		if (playerData->getCharClass() == 2) {
			cout << "He's sleeping so you have time to conjure a huge freeze spell... It freezes the dragon forever... You monster." << endl;
			cout << "Yay! You've completed the demo. Well done!" << endl;
			system("pause");
			exit(4);
		}
		else {
			cout << "What are you doing?! You aren't even a mage... You're lucky it's asleep. Do something else!" << endl;
			system("pause");
			Drake();
		}

	}

	//Generic switch case to be used in most action situations
	//Takes in player decision and calls a function
	void Consequences(void(*ActionFunction)(), void(*ActionFunction2)(), void(*ActionFunction3)(), void(*ActionFunction4)() //function with param 
	) { //A pointer called otherFunction

		switch (answer) {
		case 1:
			(*ActionFunction)(); //dereference pointer and call function it points to. Dereference uses value
								 //(*ActionFunction5)(passNumber);
		case 2:
			(*ActionFunction2)();
		case 3:
			(*ActionFunction3)();
		case 4:
			(*ActionFunction4)();
		case 5:
			CheckInventory(playerInventory);
		}
	}


	bool Action(int passValue) {

		diceResult = DiceRoll(); //call dice roll function


		if (diceResult > passValue) { //if dice result is over the pass value
			passed = true; //player passes action
			cout << "Pass" << endl;

		}
		else {
			passed = false; //otherwise player fails action
			cout << "Fail" << endl;


		}

		return passed;
	}

	void CompletedAction() {
		cout << "You've already performed this action, please select another." << endl;
	}

	void Tavern() {

		AreaDescription("You find yourself in a tavern in the middle of the day, you can hear mumbled chatter between people, laughing and toasts being held. You can look and move around it in the other window that opened with the game!!");
		answer = PickAction("Chat with bartender (1)", "Chat with Locals(2)", "Arm Wrestle(3)", "Play Poker(4)");


		switch (answer) {

		case 1:
			Consequences(&TavernBartender, NULL, NULL, NULL); //The memory address of TavernBartender
			break;

		case 2:
			passed = Action(2);

			if (passed == true) {
				cout << "The locals welcome you to the table! You all chat for a little while, they even buy you a drink" << endl;
			}
			else (passed == false || playerData->getCharRace() == 3 || playerData->getCharRace() == 4); {
				cout << "The locals look you up, down then laugh and tell you to get lost. You are most unwelcome... " << endl;
			}
			TalkWithBartender();
			Consequences(NULL, &TavernBartender, NULL, NULL);
			break;
		case 3:
			passed = Action(3);

			if (passed == true) {
				cout << "You sit down and loudly challenge the guy already sitting. He looks confused but accepts anyway... There is no competition, you take him down almost instantly" << endl;
			}
			else {
				cout << "You decided to challenge the strongest looking person in he tavern... Obviously you lose. But you put up a good fight... maybe? You're laughed back to the bar " << endl;
			}
			TalkWithBartender();
			Consequences(NULL, NULL, &TavernBartender, NULL);

			break;
		case 4:
			passed = Action(4);

			if (passed == true) {
				cout << "Your poker face is on point today and no one can read you. You win a lot of money... But spend it on drinks for everyone. Clever. " << endl;
			}
			else {
				cout << "You all start to play, however, you can't hide your delight at the cards you have... You lose, everyone leaves. Need a better pker face to make friends here. " << endl;
			}
			TalkWithBartender();
			Consequences(NULL, NULL, NULL, &TavernBartender);
			break;

		case 5:
			TalkWithBartender();
			Tavern();
			break;

		};

	};

	void OutsideTavern() {
		ClearScreen();
		cout << "You realize it's getting late so you leave the tavern to make a start on your quest.." << endl;
		AreaDescription("You feel a cool breeze as you step outside. You can hear sounds of a market vaguely. You see a dirt path leading out of the town and into a tall forest");
		answer = PickAction("Go to forest (1)", "Go to the market(2)", "-", "-");


		Consequences(Forest, Market, NULL, NULL);

	}

	void Market() {
		AreaDescription("A market...");
		answer = PickAction("Look at tome stall (1)", "Throw eggs (2)", "Ask around about Drake (3)", "Dance (4)");
		cout << "Or go back to the town square (6)" << endl;

		switch (answer) {
		case 2:
			passed = Action(3);

			if (passed == true) {
				cout << "You seem to have a natural gift for throwing eggs, you hit all the people... multiple times. They are not impressed but you're happy" << endl;
				system("pause");
			}
			else {
				cout << "You are surprisingly bad at egg throwing... Like the worst they've ever seen. How are you even going to kill this dragon?" << endl;
				system("pause");
			}
			Market();
			break;

		case 4:
			passed = Action(6);

			if (passed == true) {
				cout << "Who knew all that dancing with your mother would pay off!! No one can take their eyes off you, they've never seen such a magnificent dancer" << endl;
				system("pause");
			}
			else {
				cout << "You dance... You think you're terrific. Unfortunately no one else does, everyone stares at you, cringing." << endl;
				system("pause");
			}
			Market();
			break;
			
		case 6:
			ClearScreen();
			OutsideTavern();

		default:
			Consequences(&TomeStall, NULL, &DrakeInfo, NULL);
			break;
		}


	}

	void Forest() {
		ClearScreen();
		AreaDescription("As you walk among the giant trees, twigs snapping under your feet, you feel as though you are being watched... Your paranoid thoughts are interrupted by a group of bunnies!!");
		answer = PickAction("Forage (1)", "Chop wood with sword (2)", "Wander aimlessly (3)", "Look at animals (4)");

		Consequences(&Forage, &WoodChop, &Wander, &Animals);

		cout << "Suddenly, you fall into a large pit and black out..." << endl;
		system("pause");
		Camp();
	}

	void Camp() {
		ClearScreen();
		AreaDescription("When you awake, you see a tribe of tall, muscular men standing in front of you. Spears pointed at you. They demand to know why you are in their forest");
		answer = PickAction("Explain your quest (1)", "Kill them (2)", "-", "-");

		switch (answer) {
		case 1:
			ClearScreen();
			cout << "They are surprisingly understanding and point you in the right direction..... Yeah, right. They attack you" << endl;
			Combat(3);
			break;
		case 2:
			ClearScreen();
			Combat(3);
		}
	}

	void Cave() {
		AreaDescription("All you see is blackness until eventually, your eyes adjust.. Nope, still blackness");
		answer = PickAction("Left (1)", "Right (2)", "Listen (3)", "-");


		switch (answer) {
		case 1:
			ClearScreen();
			cout << "You walk left somewhat... More darkness" << endl;
			system("pause");
			Cave();
		case 2:
			ClearScreen();
			cout << "You walk right.. you think, you are met with.... MORE DARKNESS" << endl;
			system("pause");
			Cave();
		default:

			Consequences(NULL, NULL, &Listen, NULL);
			break;
		}
	}

	void Drake() {
		AreaDescription("You nearly don't even see it, but you feel it's breath heat the air around you. You've found the dragon...");
		answer = PickAction("Kill (1)", "Charm (2)", "Talk with (3)", "Freeze? (4)");

		Consequences(&Kill, &Charm, &Talk, &Freeze);
	}
