

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// CT4TOGA OpenGL without using GLUT - uses excerpts from here:
// http://bobobobo.wordpress.com/2008/02/11/opengl-in-a-proper-windows-app-no-glut/
// Feel free to adapt this for what you need, but please leave these comments in.

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


#pragma once

#include <windows.h>	// need this file if you want to create windows etc
#include <gl/gl.h>		// need this file to do graphics with opengl
#include <gl/glu.h>		// need this file to set up a perspective projection easily

#include <consoleapi.h>
#include <iostream>
#include <thread>

using namespace std;

// include the opengl and glu libraries
#pragma comment(lib, "opengl32.lib")	
#pragma comment(lib, "glu32.lib")


// vertex array for a simple triangle...
float myArray[9] = {
	0.05f, -0.05f, 0.0f,
	0.0f, 0.05f, 0.0f,
	-0.05f, -0.05f, 0.0f
};


float rightTriangle[9] = {
	0.05f, -0.05f, 0.0f,
	0.05f, 0.0f, 0.0f,
	-0.05f, -0.0f, 0.0f
};

float rightTriangleSecond[9] = {
	0.05f, -0.05f, 0.0f,
	-0.05f, -0.05f, 0.0f,
	-0.05f, 0.0f, 0.0f
};

//Vertex array for colors
//R,G,B and alpha values from left to right
float myColours[12] = {
	1.0f, 0.0f, 0.0f, 1.0f,
	0.0f, 1.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f, 1.0f
};

//class for the player triangle that is able to move around

class Player {
public:

	//variables for triangle co ordinates 
	float posX;
	float posY;
	float posZ;

	//booleans to check if button is still pressed so player is still moving.
	bool isMovingRight;
	bool isMovingLeft;
	bool isMovingUp;
	bool isMovingDown;

	Player() {

		posX = 0.0f;
		posY = 0.0f;
		posZ = -1.0f;


		isMovingRight = false;
		isMovingLeft = false;
		isMovingUp = false;
		isMovingDown = false;
}

	
};



// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


// function prototypes:
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow);


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

//Prototyping Menu function as it exists in game.cpp
extern void Menu();

DWORD WINAPI RunGame(LPVOID param) //Function that thread will run
{
	AllocConsole(); //Opens a console terminal alongside the OpenGL window

	//Ensures cout and cin can still be used
	FILE* pCout;
	FILE* pCin;

	freopen_s(&pCin, "CONIN$", "r", stdin);
	freopen_s(&pCout, "CONOUT$", "w", stdout);

	// run game in console
	Menu();

	return 0;
}

// In a C++ Windows app, the starting point is WinMain() rather than _tmain() or main().
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow)
{
	// some basic numbers to hold the position and size of the window
	int windowWidth = 800;
	int windowHeight = 600;
	int windowTopLeftX = 50;
	int windowTopLeftY = 50;

	// some other variables we need for our game...
	MSG msg;								// this will be used to store messages from the operating system
	bool keepPlaying = true;				// whether or not we want to keep playing



	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// this section contains all the window initialisation code, 
	// and should probably be collapsed for the time being to avoid confusion	
#pragma region  <-- click the plus/minus sign to collapse/expand!

	// this bit creates a window class, basically a template for the window we will make later, so we can do more windows the same.
	WNDCLASS myWindowClass;
	myWindowClass.cbClsExtra = 0;											// no idea
	myWindowClass.cbWndExtra = 0;											// no idea
	myWindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);	// background fill black
	myWindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);					// arrow cursor       
	myWindowClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);				// type of icon to use (default app icon)
	myWindowClass.hInstance = hInstance;									// window instance name (given by the OS when the window is created)   
	myWindowClass.lpfnWndProc = WndProc;									// window callback function - this is our function below that is called whenever something happens
	myWindowClass.lpszClassName = TEXT("my window class name");				// our new window class name
	myWindowClass.lpszMenuName = 0;											// window menu name (0 = default menu?) 
	myWindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;				// redraw if the window is resized horizontally or vertically, allow different context for each window instance

	// Register that class with the Windows OS..
	RegisterClass(&myWindowClass);

	// create a rect structure to hold the dimensions of our window
	RECT rect;
	SetRect(&rect, windowTopLeftX,				// the top-left corner x-coordinate
		windowTopLeftY,				// the top-left corner y-coordinate
		windowTopLeftX + windowWidth,		// far right
		windowTopLeftY + windowHeight);	// far left

	// adjust the window, no idea why.
	AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, false);

	// call CreateWindow to create the window
	HWND myWindow = CreateWindow(TEXT("my window class name"),		// window class to use - in this case the one we created a minute ago
		TEXT("CT4TOGA Week 19 Example"),		// window title
		WS_OVERLAPPEDWINDOW,						// ??
		windowTopLeftX, windowTopLeftY,			// x, y
		windowWidth, windowHeight,				// width and height
		NULL, NULL,								// ??
		hInstance, NULL);							// ??


	// check to see that the window created okay
	if (myWindow == NULL)
	{
		FatalAppExit(NULL, TEXT("CreateWindow() failed!")); // ch16
	}

	// if so, show it
	ShowWindow(myWindow, iCmdShow);


	// get a device context from the window
	HDC myDeviceContext = GetDC(myWindow);


	// we create a pixel format descriptor, to describe our desired pixel format. 
	// we set all of the fields to 0 before we do anything else
	// this is because PIXELFORMATDESCRIPTOR has loads of fields that we won't use
	PIXELFORMATDESCRIPTOR myPfd = { 0 };


	// now set only the fields of the pfd we care about:
	myPfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);		// size of the pfd in memory
	myPfd.nVersion = 1;									// always 1

	myPfd.dwFlags = PFD_SUPPORT_OPENGL |				// OpenGL support - not DirectDraw
		PFD_DOUBLEBUFFER |				// double buffering support
		PFD_DRAW_TO_WINDOW;					// draw to the app window, not to a bitmap image

	myPfd.iPixelType = PFD_TYPE_RGBA;					// red, green, blue, alpha for each pixel
	myPfd.cColorBits = 24;								// 24 bit == 8 bits for red, 8 for green, 8 for blue.
	// This count of color bits EXCLUDES alpha.

	myPfd.cDepthBits = 32;								// 32 bits to measure pixel depth.


	// now we need to choose the closest pixel format to the one we wanted...	
	int chosenPixelFormat = ChoosePixelFormat(myDeviceContext, &myPfd);

	// if windows didn't have a suitable format, 0 would have been returned...
	if (chosenPixelFormat == 0)
	{
		FatalAppExit(NULL, TEXT("ChoosePixelFormat() failed!"));
	}

	// if we get this far it means we've got a valid pixel format
	// so now we need to set the device context up with that format...
	int result = SetPixelFormat(myDeviceContext, chosenPixelFormat, &myPfd);

	// if it failed...
	if (result == NULL)
	{
		FatalAppExit(NULL, TEXT("SetPixelFormat() failed!"));
	}

	// we now need to set up a render context (for opengl) that is compatible with the device context (from windows)...
	HGLRC myRenderContext = wglCreateContext(myDeviceContext);

	// then we connect the two together
	wglMakeCurrent(myDeviceContext, myRenderContext);



	// opengl display setup
	glMatrixMode(GL_PROJECTION);	// select the projection matrix, i.e. the one that controls the "camera"
	glLoadIdentity();				// reset it
	gluPerspective(45.0, (float)windowWidth / (float)windowHeight, 0.1, 1000);	// set up fov, and near / far clipping planes
	glViewport(0, 0, windowWidth, windowHeight);								// make the viewport cover the whole window
	glClearColor(0.5, 0, 1.0, 1);												// set the colour used for clearing the screen
	glEnableClientState(GL_VERTEX_ARRAY);										//	turn on the ability to do vertex arrays
	glMatrixMode(GL_MODELVIEW);													// select the modelview matrix, i.e. the one that controls how local space is mapped to world space
	

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#pragma endregion

	// creates a thread and runs the RunGame function
	HANDLE game = CreateThread(NULL, 0, RunGame, NULL, 0, NULL);

	//instance of player class
	Player p1;

	p1.posX = 0.1f;
	p1.posY = -0.25f;


	// keep doing this as long as the player hasnt exited the app: 
	//MAIN GAME LOOP STARTS HERE
	while (keepPlaying == true)
	{

		// we need to listen out for OS messages.
		// if there is a windows message to process...
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// and if the message is a "quit" message...
			if (msg.message == WM_QUIT)
			{
				keepPlaying = false;	// we want to quit asap
			}
			else if (msg.message == WM_KEYDOWN) { //listens out for any key presses down
				if (msg.wParam == 'A') { //If A is pressed down....
					p1.isMovingLeft = true; //Player is/wants to move left
				}

				if (msg.wParam == 'D') {
					p1.isMovingRight = true;
				}

				if (msg.wParam == 'S') {
					p1.isMovingDown = true;
				}

				if (msg.wParam == 'W') {
					p1.isMovingUp = true;
				}
			}

			else if (msg.message == WM_KEYUP) { //Listens out for key presses up
				if (msg.wParam == 'A') { //Key pressed up....
					p1.isMovingLeft = false; //Player is/wants to stop moving
				}

				if (msg.wParam == 'D') {
					p1.isMovingRight = false;
				}

				if (msg.wParam == 'S') {
					p1.isMovingDown = false;
				}

				if (msg.wParam == 'W') {
					p1.isMovingUp = false;
				}
			}
			// or if it was any other type of message (i.e. one we don't care about), process it as normal...
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		//Update code here
		if (p1.isMovingRight == true) { //If moving right...
			p1.posX += 0.001f; //Add small amount to player x co ordinate

			if (p1.posX > 0.6f) {//If player moves off right side of screen....
				p1.posX = 0.1f; //put them back starting position
			}
		}

		if (p1.isMovingLeft == true) {
			p1.posX += -0.001f;

			if (p1.posX < -0.6f) {
				p1.posX = 0.1f;
			}
		}

		if (p1.isMovingUp == true) {
			p1.posY += 0.001f;

			if (p1.posY > 0.27f) {
				p1.posY = -0.25;
			}
		}

		if (p1.isMovingDown == true) {
			p1.posY += -0.001f;

			if (p1.posY < -0.5f) {
				p1.posY = -0.25;
			}
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// clear screen

		// put your OpenGL drawing code in this gap...

		glVertexPointer(3, GL_FLOAT, 0, myArray); //vertex data in myarray array, vertices 3d and floats
		glLoadIdentity(); //forget any translation/rotation commands
		glTranslatef(p1.posX, p1.posY, p1.posZ); //player x, y and z used. z is -1 so it will appear on screen in the frustum
		glDrawArrays(GL_TRIANGLES, 0, 3);

		float barX = 0.05f;

		do //loop to draw tavern bar
		{
			glVertexPointer(3, GL_FLOAT, 0, rightTriangle); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(barX, 0.25, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			glVertexPointer(3, GL_FLOAT, 0, rightTriangleSecond); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(barX, 0.25, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			barX += 0.05f;

		} while (barX < 0.8);

		float tabX = -0.4f;


		//loops to draw tables

		//table 1
		do 
		{
			glVertexPointer(3, GL_FLOAT, 0, rightTriangle); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, 0.1, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			glVertexPointer(3, GL_FLOAT, 0, rightTriangleSecond); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, 0.1, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			tabX += 0.05f;

		} while (tabX < -0.2);

		 tabX = -0.4f;

		do
		{
			glVertexPointer(3, GL_FLOAT, 0, rightTriangle); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, 0.05, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			glVertexPointer(3, GL_FLOAT, 0, rightTriangleSecond); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, 0.05, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			tabX += 0.05f;
			
		} while (tabX < -0.2);


		//table 2

		tabX = -0.4f;

		do
		{
			glVertexPointer(3, GL_FLOAT, 0, rightTriangle); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, -0.1, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			glVertexPointer(3, GL_FLOAT, 0, rightTriangleSecond); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, -0.1, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			tabX += 0.05f;

		} while (tabX < -0.2);

		tabX = -0.4f;

		do
		{
			glVertexPointer(3, GL_FLOAT, 0, rightTriangle); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, -0.15, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			glVertexPointer(3, GL_FLOAT, 0, rightTriangleSecond); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, -0.15, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			tabX += 0.05f;

		} while (tabX < -0.2);

	
		/////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////
		
		//Right side tables

		tabX = 0.4f;

		do
		{
			glVertexPointer(3, GL_FLOAT, 0, rightTriangle); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, 0.1, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			glVertexPointer(3, GL_FLOAT, 0, rightTriangleSecond); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, 0.1, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			tabX += 0.05f;

		} while (tabX < -0.2);

		tabX = 0.4f;

		do
		{
			glVertexPointer(3, GL_FLOAT, 0, rightTriangle); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, 0.05, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			glVertexPointer(3, GL_FLOAT, 0, rightTriangleSecond); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, 0.05, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			tabX += 0.05f;

		} while (tabX < -0.2);


		//table 2

		tabX = 0.4f;

		do
		{
			glVertexPointer(3, GL_FLOAT, 0, rightTriangle); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, -0.1, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			glVertexPointer(3, GL_FLOAT, 0, rightTriangleSecond); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, -0.1, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			tabX += 0.05f;

		} while (tabX < -0.2);

		tabX = 0.4f;

		do
		{
			glVertexPointer(3, GL_FLOAT, 0, rightTriangle); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, -0.15, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			glVertexPointer(3, GL_FLOAT, 0, rightTriangleSecond); //Pointer moves to different vertex arrays to make a right angled triangle
			glLoadIdentity();
			glTranslatef(tabX, -0.15, -1.0);
			glDrawArrays(GL_TRIANGLES, 0, 3);

			tabX += 0.05f;

		} while (tabX < -0.2);
		
		SwapBuffers(myDeviceContext);							// update graphics

		// Puts rendering thread to sleep for 1ms, ensures other thread won't get stuck
		Sleep(1);
	}


	// the next bit will therefore happen when the player quits the app,
	// because they are trapped in the previous section as long as (keepPlaying == true).

	// UNmake our rendering context (make it 'uncurrent')
	wglMakeCurrent(NULL, NULL);

	// delete the rendering context, we no longer need it.
	wglDeleteContext(myRenderContext);

	// release our window's DC from the window
	ReleaseDC(myWindow, myDeviceContext);

	// end the program
	return msg.wParam;
}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// this part contains some code that should be collapsed for now too...
#pragma region keep_this_bit_collapsed_too!

// this function is called when any events happen to our window
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{

	switch (message)
	{
		// if they exited the window...	
	case WM_DESTROY:
		// post a message "quit" message to the main windows loop
		PostQuitMessage(0);
		return 0;
		break;
	}

	// must do this as a default case (i.e. if no other event was handled)...
	return DefWindowProc(hwnd, message, wparam, lparam);

}

#pragma endregion
