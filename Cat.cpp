#include "stdafx.h"
#include "Cat.h"

Cat::Cat() {
	hp = 20;
	str = 4;
	def = 3;

	switch (getCharClass()) {

	case 1:
		finalDMG = str + Warrior_Traits.weaponDMG;
		finalHP = hp + Warrior_Traits.hpMod;
		break;
	case 2:
		finalDMG = str + Mage_Traits.weaponDMG;
		break;
	case 3:
		finalDMG = str + Ranger_Traits.weaponDMG;
		break;
	case 4:
		finalDMG = str + Tamer_Traits.weaponDMG;
		break;
	}
}