#include "stdafx.h"
#include "Enemy.h"

Enemy::Enemy() {
	str = 4;
	def = 2;
	hp = 8;
}

int Enemy::getStr() {
	return str;
}

int Enemy::getDef() {
	return def;
}

int Enemy::getHp() {
	return hp;
}

void Enemy::setHp(int theHp) {
	hp = theHp;
}

void Enemy::takeDmg(int amount) {
	hp -= amount;

	if (hp < 0) {
		hp = 0;
	}
}

bool Enemy::isDead() {
	return (hp <= 0);
}